const search = require('@koa/router')()
const DB = require('../controllers/db')

search.post('/user', async (ctx, next) => {
  const msg = await DB.searchUsers(ctx.request.body.target, ctx.request.body.count)
  ctx.body = msg
})

search.post('/video', async (ctx, next) => {
  const msg = await DB.searchVideos(ctx.request.body.target, ctx.request.body.count)
  ctx.body = msg
})




module.exports = search