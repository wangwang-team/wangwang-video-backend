const router = require('@koa/router')()

const home = require('./home')
const login = require('./login')
const register = require('./register')
const favour = require('./favour')
const userwork = require('./userWork')
const user = require('./user')
const search = require('./search')
const recommend = require('./recommend')



router.redirect('/', '/home')

// 使用路由
router.use('/home', home.routes(), home.allowedMethods())
router.use('/login', login.routes(), login.allowedMethods())
router.use('/register', register.routes(), register.allowedMethods())
router.use('/favour', favour.routes(), favour.allowedMethods())
router.use('/userwork', userwork.routes(), userwork.allowedMethods())
router.use('/user', user.routes(), user.allowedMethods())
router.use('/search', search.routes(), search.allowedMethods())
router.use('/recommend', recommend.routes(), recommend.allowedMethods())



module.exports = router