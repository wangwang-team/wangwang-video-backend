const login = require('@koa/router')()
const DB = require('../controllers/db')

login.post('/', async (ctx, next) => {
  const msg = await DB.userLogin(ctx.request.body.userName, ctx.request.body.userPwd)
  ctx.body = msg
})



module.exports = login