const recommend = require('@koa/router')()
const DB = require('../controllers/db')

recommend.post('/', async (ctx, next) => {
  ctx.body = await DB.recommendVideos(ctx.request.body.userName || ctx.request.header.host)
})

recommend.post('/more', async (ctx, next) => {
  if (ctx.request.body.videoId) {
    const msg = await DB.recommendVideo(ctx.request.body.userName || ctx.request.header.host, ctx.request.body.videoId)
    ctx.body = msg
  } else {
    ctx.body = {
      code: 408,
      msg: `未携带必要参数`
    }
  }
})



module.exports = recommend