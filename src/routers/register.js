const register = require('@koa/router')()
const DB = require('../controllers/db')

register.post('/', async (ctx, next) => {
  if (ctx.request.body.userName && ctx.request.body.userPwd && ctx.request.body.nickName) {
    const msg = await DB.userRegister(ctx.request.body.userName, ctx.request.body.userPwd, ctx.request.body.nickName)
    ctx.body = msg
  } else {
    ctx.body = {
      code: 408,
      msg: `未携带必要参数`
    }
  }
})



module.exports = register