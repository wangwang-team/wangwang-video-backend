const path = require('path');
const koaBody = require('koa-body');
// const koaStatic = require('koa-static');
const koaCors = require('koa-cors')
const Koa = require('koa');
const app = new Koa();

const router = require('./routers')

// 中间件
app.use(koaBody());  // 请求体 parse 中间件，用于 parse json 格式请求体
// app.use(koaCors())  // 允许跨域


// 路由引入
app.use(router.routes(), router.allowedMethods())

module.exports = app;


// BaseUrl = https://qc8eoi.app.cloudendpoint.cn




