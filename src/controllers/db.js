const dbUtils = require('../utils/dbUtils')
const tk = require('../utils/token')
const RE = require('../utils/regex')
const reUtils = require('../utils/reUtils')
const EM = require('../utils/email')
const { getUserDetail } = require('../utils/getUserDetail')



// 用户登录查找
const userLogin = async function(userName, userPwd) {
  if (!userName || !userPwd) {
    return {
      code: 400,
      data: {
        msg: `用户或密码不能为空`
      }
    }
  }
  const userInfo = await dbUtils.findUserByUserName(userName)
  if (userInfo && JSON.stringify(userInfo) != "{}") {
    if (userInfo.userpwd == userPwd) {
      const token = await tk.setToken(userName)
      return {
        code: 200,
        data: {
          msg: `登录成功`,
          userName: userInfo.username,
          nickName: userInfo.nickname,
          userId: userInfo._id,
          avatar: userInfo.avatar,
          token
        }
      }
    } else {
      return {
        code: 401,
        data: {
          msg: `密码错误`
        }
      }
    }
  } else {
    return {
      code: 402,
      data: {
        msg: `用户未找到`
      }
    }
  }
}


// 用户注册创建
const userRegister = async function(userName, userPwd, userNickName) {
  const curr = await dbUtils.findUserByUserName(userName)
  if (curr && JSON.stringify(curr) != "{}") {
    return {
      code: 400,
      data: {
        msg: `账号已存在`
      }
    }
  }
  const user = await dbUtils.addUser(userName, userPwd, userNickName)
  return {
    code: 200,
    data: {
      msg: `注册成功`,
      userName: user.username,
      nickName: user.nickname,
      userId: user._id
    }
  }
}


// 发送用户验证码
const sendEmailtoUser = async function(userName) {
  const passCode = await tk.createCodeByUserName(userName)
  const emailMsg = await EM.sendEmail(userName, passCode)
  return {
    code: 200,
    data: {
      msg: `验证码创建成功`,
      passCode,
      emailMsg
    }
  }
}


// 用户密码修改
const userChangePassword = async function(userName, userPwd, userNewPwd) {
  const loginInfo = await userLogin(userName, userPwd)
  if (loginInfo.code === 200) {
    if (userPwd != userNewPwd) {
      const user = await dbUtils.changePasswordByUserName(userName, userNewPwd)
      return {
        code: 200,
        data: {
          msg: `修改成功`,
          userName: user.username,
          nickName: user.nickname,
          userId: user._id
        }
      }
    } else {
      return {
        code: 403,
        data: {
          msg: `新旧密码不能相同`
        }
      }
    }
  } else {
    return loginInfo
  }
}


// 用户详情信息
const userDetail = async function(userName) {
  const userDetailInfo = await getUserDetail(userName)
  return {
    code: 200,
    data: {
      msg: `查找成功`,
      userDetailInfo
    }
  }
}


// 用户昵称修改
const userChangeNickName = async function(userId, nickName) {
  const userInfo = await dbUtils.changeNickNameByUserId(userId, nickName)
  return {
    code: 200,
    data: {
      msg: `修改成功`,
      nickName: userInfo.nickname
    }
  }
}


// 用户头像修改
const userChangeAvatar = async function(userId, avatar) {
  const userInfo = await dbUtils.changeAvatarByUserId(userId, avatar)
  return {
    code: 200,
    data: {
      msg: `修改成功`,
      avatar: userInfo.avatar
    }
  }
}


// 用户签名修改
const userChangeSign = async function(userId, sign) {
  const userInfo = await dbUtils.changeSignByUserId(userId, sign)
  return {
    code: 200,
    data: {
      msg: `修改成功`,
      sign: userInfo.sign
    }
  }
}


// 用户作品列表
const userWorks = async function(userName) {
  const userInfo = await dbUtils.findUserByUserName(userName)
  const worksList = await dbUtils.findWorksByAuthorId(userInfo._id)
  const count = worksList.length
  return {
    code: 200,
    data: {
      msg: `查找成功`,
      count,
      worksList
    }
  }
}


// 单条用户作品删除
const deleteUserWork = async function(videoId) {
  const res = await dbUtils.deleteVideoByVideoId(videoId)
  return {
    code: 200,
    data: {
      msg: `删除成功`,
      res
    }
  }
}


// 单条用户作品增加
const addUserWork = async function(title, author, videoUrl, cover, description) {
  const res = await dbUtils.addVideo(title, author, videoUrl, cover, description)
  return {
    code: 200,
    data: {
      msg: `上传成功`,
      res
    }
  }
}


// 用户收藏列表
const userFavorites = async function(userName) {
  const userInfo = await dbUtils.findUserByUserName(userName)
  const favoritesList = await dbUtils.findFavoritesByUserId(userInfo._id)
  let favoritesInfoList = []
  if (favoritesList && favoritesList.length) {  
    for (const favour of favoritesList) {
      const workInfo = await dbUtils.findWorkByVideoId(favour.videoid)
      const authorInfo = await dbUtils.findUserByUserId(workInfo.author)
      workInfo.authorUserName = authorInfo.username
      workInfo.authorNickName = authorInfo.nickname
      workInfo.authorAvatar = authorInfo.avatar
      workInfo.collectTime = favour.createdAt
      favoritesInfoList.push(workInfo)
    }
  }
  return {
    code: 200,
    data: {
      msg: `查找成功`,
      count: favoritesInfoList.length,
      favoritesInfoList
    }
  }
}


// 单条用户收藏删除
const deleteUserFavour = async function(videoId, userId) {
  const res = await dbUtils.deleteFavourByVideoId(videoId, userId)
  return {
    code: 200,
    data: {
      msg: `删除成功`,
      res
    }
  }
}


// 单条用户收藏增加
const addUserFavour = async function(videoId, userId) {
  const res = await dbUtils.addFavour(videoId, userId)
  return {
    code: 200,
    data: {
      msg: `增加成功`,
      res
    }
  }
}


// 搜索用户
const searchUsers = async function(target, count) {
  const userList = await RE.findUsers(target, count)
  return {
    code: 200,
    data: {
      msg: `搜索成功`,
      userList
    }
  }
}


// 搜索视频
const searchVideos = async function(target, count) {
  const videoList = await RE.findVideos(target, count)
  return {
    code: 200,
    data: {
      msg: `搜索成功`,
      videoList
    }
  }
}


// 推荐视频
const recommendVideos = async function(userName) {
  const videoList = await reUtils.allVideos(userName)
  return {
    code: 200,
    data: {
      msg: `查找成功`,
      videoList
    }
  }
}


// 推荐更多视频
const recommendVideo = async function(userName, videoId) {
  const videoInfo = await reUtils.moreVideo(userName, videoId)
  return {
    code: 200,
    data: {
      msg: `返回成功`,
      videoInfo
    }
  }
}




module.exports = {
  userLogin,
  userRegister,
  sendEmailtoUser,
  userChangePassword,
  userDetail,
  userChangeNickName,
  userChangeAvatar,
  userChangeSign,
  userWorks,
  deleteUserWork,
  addUserWork,
  userFavorites,
  deleteUserFavour,
  addUserFavour,
  searchUsers,
  searchVideos,
  recommendVideos,
  recommendVideo
}