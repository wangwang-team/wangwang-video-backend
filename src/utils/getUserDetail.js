const dbUtils = require('../utils/dbUtils')
const { formatDate } = require('../utils/formatDate')


const getUserDetail = async function(userName) {
  const userInfo = await dbUtils.findUserByUserName(userName)
  const worksList = await dbUtils.findWorksByAuthorId(userInfo._id)
  const lovedList = await dbUtils.findLovedListByUserId(userInfo._id)
  // const favoritesList = await dbUtils.findFavoritesByUserId(userInfo._id)
  const loginTimesList = await dbUtils.findLoginTimesByUserName(userName)
  
  const worksListByMouth = new Array(12).fill(0)
  const lovedListByWeek = new Array(7).fill(0)
  const loginTimesByHour = new Array(8).fill(0)
  const worksInfoList = []
  const nowTime = formatDate(new Date())
  const dayTime = new Date(`${nowTime.getFullYear()}/${nowTime.getMonth() + 1}/${nowTime.getDate()}`)
  const oneDay = 86400000
  let totalLove = 0
  
  const lastLoginTime = formatDate(new Date(loginTimesList[0].createdAt))

  worksList.map(work => {
    const workDate = formatDate(new Date(work.createdAt))
    if (nowTime.getFullYear() === workDate.getFullYear()) {
      worksListByMouth[workDate.getMonth()]++
    }
    worksInfoList.push({
      videoId: work._id,
      date: `${workDate.getFullYear()}-${workDate.getMonth() + 1}-${workDate.getDate()}`,
      title: work.title,
      favorNum: work.love
    })
    totalLove += work.love
  })

  lovedList.map(loved => {
    const lovedDate = formatDate(new Date(loved.createdAt))
    if (lovedDate - dayTime.getTime() < oneDay) {
      lovedListByWeek[6]++
    } else if (lovedDate - dayTime.getTime() < oneDay * 2) {
      lovedListByWeek[5]++
    } else if (lovedDate - dayTime.getTime() < oneDay * 3) {
      lovedListByWeek[4]++
    } else if (lovedDate - dayTime.getTime() < oneDay * 4) {
      lovedListByWeek[3]++
    } else if (lovedDate - dayTime.getTime() < oneDay * 5) {
      lovedListByWeek[2]++
    } else if (lovedDate - dayTime.getTime() < oneDay * 6) {
      lovedListByWeek[1]++
    } else if (lovedDate - dayTime.getTime() < oneDay * 7) {
      lovedListByWeek[0]++
    }
  })

  loginTimesList.map(time => {
    const loginTime = formatDate(new Date(time.createdAt))
    if (loginTime.getHours() < 3) {
      loginTimesByHour[0]++
    } else if (loginTime.getHours() % 24 < 6) {
      loginTimesByHour[1]++
    } else if (loginTime.getHours() < 9) {
      loginTimesByHour[2]++
    } else if (loginTime.getHours() < 12) {
      loginTimesByHour[3]++
    } else if (loginTime.getHours() < 15) {
      loginTimesByHour[4]++
    } else if (loginTime.getHours() < 18) {
      loginTimesByHour[5]++
    } else if (loginTime.getHours() < 21) {
      loginTimesByHour[6]++
    } else if (loginTime.getHours() < 24) {
      loginTimesByHour[7]++
    }
  })

  return {
    lineData: worksListByMouth,  // 发布作品每月统计
    barData: lovedListByWeek,  // 最近一周被收藏统计
    pieData: loginTimesByHour,  // 登录时间分布
    tableData: worksInfoList.reverse(),  // 已发布作品  .slice(0, 5)
    cardData: {
      lastLoginTime: `${lastLoginTime.getMonth() + 1}-${lastLoginTime.getDate()}-${lastLoginTime.getHours()}:${lastLoginTime.getMinutes()}`,  // 上次登录时间
      publishNum: worksList.length,  // 发布作品总数
      favorNum: totalLove  // 总共作品被收藏数
    },
    userData: {
      userId: userInfo._id,
      userName: userInfo.username,
      userNickName: userInfo.nickname,
      avatar: userInfo.avatar,
      sign: userInfo.sign
    }
  }
}



module.exports = {
  getUserDetail
}