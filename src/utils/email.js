const nodemailer = require('nodemailer');

const sendEmail = async function(emailUrl, passCode) {
  //开启SMTP连接池
  let transporter = nodemailer.createTransport({
    host: 'smtp.qq.com',
    secureConnection: true,  //use SSL
    port: 465,
    secure: true,  //secure: true for port 465, secure:false for port 587
    auth: {
        user: '1092600370@qq.com',
        pass: 'dfvhnnqbotrxigcg'  //qq授权码
    }
  });

  //设置邮件内容（谁发送什么给谁）
  let mailOptions = {
    from: '"汪汪video"<1092600370@qq.com>',  //发件人
    to: emailUrl,   //收件人
    subject: 'Hello',  //主题
    text: `欢迎注册汪汪video视频，注册验证码为 ${passCode} 。`,  //文本内容
    html: `<b>欢迎注册汪汪video视频，注册验证码为 ${passCode} 。`,  //html body
  };

  //使用先前创建的传输器的sendMail方法传递消息对象
  let sendMsg
  transporter.sendMail(mailOptions, (error, info) => {
    if(error) {
      sendMsg = `发送失败：${error}`
    } else {
      sendMsg = `邮箱发送成功`
    }
  });

  return {
    from: mailOptions.from,
    to: mailOptions.to,
    sendMsg
  }
}



module.exports = {
  sendEmail
}