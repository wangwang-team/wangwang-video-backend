const inspirecloud = require('@byteinspire/api')
const ObjectId = inspirecloud.db.ObjectId


// 通过userid查找用户
const findUserByUserId = async function(userId) {
  const Users = inspirecloud.db.table('users');
  const userInfo = await Users.where({_id: ObjectId(userId)}).findOne();  // String
  return userInfo
}


// 通过username查找用户
const findUserByUserName = async function(userName) {
  const Users = inspirecloud.db.table('users');
  const userInfo = await Users.where({username: userName}).findOne();
  return userInfo
}


// 增加一位用户
const addUser = async function(userName, userPwd, userNickName) {
  const Users = inspirecloud.db.table('users');
  const user = Users.create({username: userName, userpwd: userPwd, nickname: userNickName});
  await Users.save(user)
  return user
}


// 通过username修改密码
const changePasswordByUserName = async function(userName, userNewPwd) {
  const Users = inspirecloud.db.table('users');
  const user = await Users.where({username: userName}).findOne();
  user.userpwd = userNewPwd
  await Users.save(user)
  return user
}


// 通过userid修改昵称
const changeNickNameByUserId = async function(userId, userNickName) {
  const Users = inspirecloud.db.table('users');
  const user = await Users.where({_id: ObjectId(userId)}).findOne();  // String
  user.nickname = userNickName
  await Users.save(user)
  return user
}


// 通过userid修改头像
const changeAvatarByUserId = async function(userId, avatar) {
  const Users = inspirecloud.db.table('users');
  const user = await Users.where({_id: ObjectId(userId)}).findOne();  // String
  user.avatar = avatar
  await Users.save(user)
  return user
}


// 通过userid修改签名
const changeSignByUserId = async function(userId, sign) {
  const Users = inspirecloud.db.table('users');
  const user = await Users.where({_id: ObjectId(userId)}).findOne();  // String
  user.sign = sign
  await Users.save(user)
  return user
}


// 通过author查找作品
const findWorksByAuthorId = async function(authorId) {
  const Videos = inspirecloud.db.table('videos');
  const worksList = await Videos.where({author: String(authorId)}).find();  // ObjectId
  return worksList
}


// 通过videoid查找作品
const findWorkByVideoId = async function(videoId) {
  const Videos = inspirecloud.db.table('videos');
  const workInfo = await Videos.where({_id: ObjectId(videoId)}).findOne();  // String
  return workInfo
}


// 通过videoid删除单条作品
const deleteVideoByVideoId = async function(videoId) {
  const Videos = inspirecloud.db.table('videos');
  const Favorites = inspirecloud.db.table('favorites');
  const FavoritesList = await Favorites.where({videoid: videoId}).find();
  for (const favour of FavoritesList) {
    await Favorites.delete(favour)
  }
  const workInfo = await Videos.where({_id: ObjectId(videoId)}).findOne();  // String
  const result = await Videos.delete(workInfo)
  return result
}


// 增加单条作品
const addVideo = async function(title, author, videoUrl, cover, description) {
  const Videos = inspirecloud.db.table('videos');
  const videoInfo = Videos.create({title: title, author: author, url: videoUrl, cover: cover, desc: description, love: 0});
  await Videos.save(videoInfo)
  return videoInfo
}


// 通过userid查找收藏
const findFavoritesByUserId = async function(userId) {
  const Favorites = inspirecloud.db.table('favorites');
  const favoritesList = await Favorites.where({userid: String(userId)}).find();  // ObjcetId
  return favoritesList
}


// 通过videoid删除单条收藏
const deleteFavourByVideoId = async function(videoId, userId) {
  const Favorites = inspirecloud.db.table('favorites');
  const Videos = inspirecloud.db.table('videos');
  const favourInfo = await Favorites.where({videoid: videoId, userid: userId}).findOne(); 
  const result = await Favorites.delete(favourInfo);
  const videoInfo = await Videos.where({_id: ObjectId(videoId)}).findOne();
  videoInfo.love--;
  await Videos.save(videoInfo);
  return result;
}


// 增加单条收藏
const addFavour = async function(videoId, userId) {
  const Favorites = inspirecloud.db.table('favorites');
  const Videos = inspirecloud.db.table('videos');
  const result = await Favorites.save({userid: userId, videoid: videoId});
  const videoInfo = await Videos.where({_id: ObjectId(videoId)}).findOne();
  videoInfo.love++;
  await Videos.save(videoInfo);
  return result;
}


// 判断用户是否收藏了当前视频
const isLoveVideo = async function(userId, videoId) {
  const Favorites = inspirecloud.db.table('favorites');
  const favourInfo = await Favorites.where({userid: String(userId), videoid: String(videoId)}).findOne()
  if (favourInfo && JSON.stringify(favourInfo) != "{}") {
    return true
  }
  return false
}


// 通过userid查找全部作品被收藏信息
const findLovedListByUserId = async function(userId) {
  const Videos = inspirecloud.db.table('videos');
  const worksList = await Videos.where({author: String(userId)}).find();  // ObjectId
  const Favorites = inspirecloud.db.table('favorites');
  let lovedList = []
  for (const work of worksList) {
    const favoritesList = await Favorites.where({videoid: String(work._id)}).find();  // ObjcetId
    lovedList.push(...favoritesList)
  }
  return lovedList
}


// 通过username查找登录时间信息
const findLoginTimesByUserName = async function(userName) {
  const UserLoginTime = inspirecloud.db.table('user-login-time');
  const loginTimesList = await UserLoginTime.where({username: userName}).sort({createdAt: -1}).find();
  return loginTimesList
}


// 通过videoid查找用户观看记录
const findWatchedByVideoId = async function(videoId) {
  const Watched = inspirecloud.db.table('watched');
  const watchedInfo = await Watched.where({videoid: String(videoId)}).findOne()  // ObjectId
  return watchedInfo
}


// 通过username查找用户观看记录
const findWatchedListByUserName = async function(userName) {
  const Watched = inspirecloud.db.table('watched');
  const watchedList = await Watched.where({username: userName}).find()  // ObjectId
  return watchedList
}


// 增加单条观看记录
const addWatched = async function(userName, videoId) {
  const Watched = inspirecloud.db.table('watched');
  const watchedInfo = Watched.create({username: userName, videoid: String(videoId)});  // ObjectId
  await Watched.save(watchedInfo)
  return watchedInfo
}




module.exports = {
  findUserByUserId,
  findUserByUserName,
  addUser,
  changePasswordByUserName,
  changeNickNameByUserId,
  changeAvatarByUserId,
  changeSignByUserId,
  findWorksByAuthorId,
  findWorkByVideoId,
  deleteVideoByVideoId,
  addVideo,
  findFavoritesByUserId,
  deleteFavourByVideoId,
  addFavour,
  isLoveVideo,
  findLovedListByUserId,
  findLoginTimesByUserName,
  findWatchedByVideoId,
  findWatchedListByUserName,
  addWatched
}

