const getNowFormatDate = function() {
  let date = new Date()
  let year = date.getFullYear()
  let mouth = date.getMonth() + 1
  let day = date.getDate()
  if (mouth >= 1 && mouth <= 9) {
    mouth = '0' + mouth
  }
  if (day >= 0 && day <= 9) {
    day = '0' + day
  }
  let currentDate = `${year}年${mouth}月${day}日`
  return currentDate
}

module.exports = {
  getNowFormatDate
}