const inspirecloud = require('@byteinspire/api')
const db = inspirecloud.db
const dbUtils = require('../utils/dbUtils')


// 最初返回视频
const allVideos = async function(userName) {
  const Videos = inspirecloud.db.table('videos');
  const userInfo = await dbUtils.findUserByUserName(userName) || {}
  if (!userInfo || JSON.stringify(userInfo) === "{}") {
    userInfo._id = `unloginUserId-${userName}`
    userInfo.username = `unloginUserName-${userName}`
  }
  const count = 5  // 一次传的视频数量
  // const from = new Date('2022-01-01 00:00:00+08');  // 限制日期
  // const totalVideos =  await Videos.where({createdAt: db.gt(from)}).find();
  const totalVideos =  await Videos.where().find();
  const filterVideosList = []
  const watchedList = await dbUtils.findWatchedListByUserName(userInfo.username)
  const watchedSet = new Set()
  for (const watched of watchedList) {
    watchedSet.add(watched.videoid)
  }
  for (const video of totalVideos) {
    if (!watchedSet.has(`${video._id}`)) {
      filterVideosList.push(video)
    }
  }
  if (filterVideosList.length === 0) {
    return `所有视频都看完了`
  }
  const randomVideosList = []
  if (filterVideosList.length > count) {
    while (randomVideosList.length < count) {
      const index = Math.floor(Math.random() * filterVideosList.length)
      randomVideosList.push(...filterVideosList.splice(index, 1))
    }
  } else {
    randomVideosList = filterVideosList
  }
  const videoList = []
  for (const video of randomVideosList) {
    const authorInfo = await dbUtils.findUserByUserId(video.author)
    videoList.push({
      videoId: video._id,
      videoUrl: video.url,
      cover: video.cover,
      title: video.title,
      authorId: video.author,
      authorNickName: authorInfo.nickname,
      authorUserName: authorInfo.username,
      description: video.desc,
      love: video.love,
      ifPlay: false,
      isLove: await dbUtils.isLoveVideo(userInfo._id, video._id)
    })
  }
  if (videoList.length > 0) {
    videoList[0].ifPlay = true
  }
  return videoList
}


// 之后返回视频
const moreVideo = async function(userName, videoId) {
  const Videos = inspirecloud.db.table('videos');
  const userInfo = await dbUtils.findUserByUserName(userName) || {}
  if (!userInfo || JSON.stringify(userInfo) === "{}") {
    userInfo._id = `unloginUser-${userName}`
    userInfo.username = `unloginUser-${userName}`
  }
  await dbUtils.addWatched(userInfo.username, videoId)
  let videoInfo = {
    author: "",
    _id: ""
  }       
  // const from = new Date('2022-01-01 00:00:00+08');  // 限制日期
  // const totalVideos =  await Videos.where({createdAt: db.gt(from)}).find();
  const totalVideos =  await Videos.where().find();
  const filterVideosList = []
  const watchedList = await dbUtils.findWatchedListByUserName(userInfo.username)
  const watchedSet = new Set()
  for (const watched of watchedList) {
    watchedSet.add(watched.videoid)
  }
  for (const video of totalVideos) {
    if (!watchedSet.has(`${video._id}`)) {
      filterVideosList.push(video)
    }
  }
  if (filterVideosList && filterVideosList.length) {
    const index = Math.floor(Math.random() * filterVideosList.length)
    videoInfo = filterVideosList[index]
  } else {
    return `所有视频都看完了`
  }
  const authorInfo = await dbUtils.findUserByUserId(videoInfo.author)
  return {
    videoId: videoInfo._id,
    videoUrl: videoInfo.url,
    cover: videoInfo.cover,
    title: videoInfo.title,
    authorId: videoInfo.author,
    authorNickName: authorInfo.nickname,
    authorUserName: authorInfo.username,
    description: videoInfo.desc,
    love: videoInfo.love,
    ifPlay: false,
    isLove: await dbUtils.isLoveVideo(userInfo._id, videoInfo._id)
  }
}



module.exports = {
  allVideos,
  moreVideo
}