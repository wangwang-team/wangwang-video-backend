function formatDate(time) {
  let newTime = new Date(time)
  const chinaTime = new Date(newTime.getTime() + 28800000)
  return chinaTime
}

module.exports = {
  formatDate
}